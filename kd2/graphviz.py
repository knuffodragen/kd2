from pathlib import Path
import subprocess
from urllib.parse import quote


CACHE = {}  # Runtime cache to save some re-rendering


def render(dot_code: str) -> Path:
    """Render a dot source string as a HTML image"""
    name = hash(dot_code)
    if name in CACHE:
        return CACHE[name]
    try:
        process = subprocess.run(["dot", "-Tsvg"], input=dot_code.encode(),
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except FileNotFoundError:
        raise RuntimeError("Graphviz not found!")
    if process.returncode != 0:
        raise RuntimeError(f"Graphviz error! {process.stderr.decode()}")
    encoded = "data:image/svg+xml," + quote(process.stdout.decode())
    image = CACHE[name] = f'<img src="{encoded}">'
    return image
