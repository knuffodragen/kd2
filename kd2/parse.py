"""
This module contains the parser that turns plain wiki text into logical tokens.
This is the first step in rendering the page to HTML.

TODO It would be nice to replace this with some neat standard parser lib
but my limited attempts have run into problems because the wiki syntax is
full of corner cases.
"""

import re
from typing import Tuple, List, Union


WIKI_WORD = r"\b([A-ZÅÄÖ]{1,2}[a-zåäö0-9]+){2,}\b"
URL = r"http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?"


class T:
    """ Token types """
    NOFORMAT = "NOFORMAT"  # pragma: no mutate
    MATH = "MATH"  # pragma: no mutate
    BOLD = "BOLD"  # pragma: no mutate
    ITALIC = "ITALIC"  # pragma: no mutate
    CODE = "CODE"  # pragma: no mutate
    UNDERSCORE = "UNDERSCORE"  # pragma: no mutate
    SUB = "SUB"  # pragma: no mutate
    SUB2 = "SUB2"  # pragma: no mutate
    SUP = "SUP"  # pragma: no mutate
    SUP2 = "SUP2"  # pragma: no mutate
    SMALLER = "SMALLER"  # pragma: no mutate
    BIGGER = "BIGGER"  # pragma: no mutate
    WIKI_WORD = "WIKI_WORD"  # pragma: no mutate
    URL = "URL"  # pragma: no mutate
    COMMAND = "COMMAND"  # pragma: no mutate

    VERBATIM = "VERBATIM"  # pragma: no mutate
    COMMENT = "COMMENT"  # pragma: no mutate
    DISPLAYMATH = "DISPLAYMATH"  # pragma: no mutate
    CITE = "CITE"  # pragma: no mutate
    LIST = "LIST"  # pragma: no mutate
    NUMBERED_LIST = "NUMBERED_LIST"  # pragma: no mutate
    DEFINITION_LIST = "DEFINITION_LIST"  # pragma: no mutate
    TERM = "TERM"  # pragma: no mutate
    DESC = "DESC"  # pragma: no mutate
    LIST_ITEM = "LIST_ITEM"  # pragma: no mutate
    TABLE = "TABLE"  # pragma: no mutate
    TABLE_ROW = "TABLE_ROW"  # pragma: no mutate
    TABLE_CELL = "TABLE_CELL"  # pragma: no mutate
    HLINE = "HLINE"  # pragma: no mutate
    PARAGRAPH = "PARAGRAPH"  # pragma: no mutate
    COMMAND_BLOCK = "COMMAND_BLOCK"  # pragma: no mutatea


LINE_PATTERN = (
    r"\\\\(?P<NOFORMAT>.*?)(\\\\|$)"
    r"|\$(?P<MATH>[^\n].*?)\$"
    r"|##(?P<BOLD>\S|(\S.*?\S))(##|$)"
    r"|''(?P<ITALIC>\S|(\S.*?\S))(''|$)"
    r"|==(?P<CODE>\S|(\S.*?\S))(==|$)"
    r"|__(?P<UNDERSCORE>\S|(\S.*?\S))(__|$)"
    r"|,,(?P<SUB>\S|(\S.*?\S))(,,|$)"
    r"|_\{(?P<SUB2>.*?)\}"
    r"|\^\^(?P<SUP>\S|(\S.*?\S))(\^\^|$)"
    r"|\^\{(?P<SUP2>.*?)\}"
    r"|--(?P<SMALLER>\S|(\S.*?\S))(--|$)"
    r"|\+\+(?P<BIGGER>\S|(\S.*?\S))(\+\+|$)"
    rf"|(?P<WIKI_WORD>{WIKI_WORD})"
    rf"|(?P<URL>({URL})"
    r"|\[\[(?P<COMMAND>.*?)\]\]"
)

MULTILINE_PATTERN = (
    r"\\\\(?P<NOFORMAT>(.*?(\n|\Z)?)+)\\\\"
    r"|(?P<VERBATIM>^==( *)$(.*?(\n|\Z)?)+^==( *)$)"
    r"|^\[\[(?P<COMMAND_BLOCK>\w+=( *)$(.*?(\n|\Z)?)+)^\]\]( *)$"
    r"|(?P<COMMENT>(^[>]+.*?$)+)(^[.]$)?"
    r"|(?P<CITE>(^[ ].*?(\n+|\Z))+)(^[.]$)?"
    r"|(?P<DISPLAYMATH>^\$\$.*?\$\$$)"
    r"|(?P<LIST>(^[ >]*[*> ].*?(\n+|\Z))+)(^[.]$)?"
    r"|(?P<NUMBERED_LIST>(^[ >]*[0> ].*?(\n+|\Z))+)(^[.]$)?"
    r"|(?P<DEFINITION_LIST>(\?(?P<TERM>.*?)\n+([! ](?P<DESC>.*?)(\n+|\Z))+)+)(^[.]$)?"
    r"|(?P<TABLE>(^\|\|(.*?\|\|)+(\n|\Z))+)"
    r"|(?P<HLINE>^----\s*$)"
    r"|(?P<PARAGRAPH>^.*?$)"
)


# MyPy does not currently support recursive type declarations so this
# is a fairly weak typing. Seems like it's in the works though
# (https://github.com/python/mypy/issues/731)
TokenValue = Union[None, str, List]  # pragma: no mutate
Token = Union[Tuple[str, TokenValue], Tuple[str, TokenValue, Tuple[int, int]]]  # pragma: no mutate
TokenList = List[Union[str, Token]]  # pragma: no mutate


def parse_line(line: str) -> TokenList:
    "Parse a single line into a nested list of (type, content) tokens."
    tokens: TokenList = []
    pos = 0
    for match in re.finditer(LINE_PATTERN, line):
        value: Union[str, TokenList]
        for typ, value in match.groupdict().items():
            if value is not None:
                break

        before = line[pos:match.start()]
        if before:
            tokens.append(before)
        if typ in {T.NOFORMAT, T.CODE, T.MATH, T.URL, T.WIKI_WORD, T.COMMAND}:
            if value:
                tokens.append((typ, [value]))
        else:
            value = parse_line(value)
            if value:
                tokens.append((typ, value))
        pos = match.end()
    after = line[pos:]
    if after:
        tokens.append(after)
    return tokens


def itemize(token: Token, item_typ: str) -> Token:
    typ, content = token
    if typ in (T.PARAGRAPH,):
        return (item_typ, content)
    return token


def parse(text: str, parent_start=0, level=0) -> List[Token]:

    "Parse a multiline text into tokens."

    # TODO the span handling is a bit messy (but works)
    tokens: List[Token] = []
    start_offset = 0

    while text.startswith(" "):
        start_offset += 1
        text = text[1:]

    for match in re.finditer(MULTILINE_PATTERN, text, flags=re.MULTILINE):
        for typ, part in match.groupdict().items():
            if part is not None:
                break

        end = match.end(typ)
        if part.endswith("\n"):
            # Multiline stuff will always end with a newline. However for
            # snippet editing, we need to exclude the last linebreak, because
            # removing it will lead to merging with whatever comes next, which
            # is very confusing. So, in case the match ends with a newline,
            # we back off the span by one.
            # TODO there should be a better way to detect multiline
            end -= 1
        span = (parent_start + start_offset + match.start(typ),
                parent_start + start_offset + end)

        if typ == T.COMMENT:
            s = "\n".join(l[1:] for l in part.splitlines())
            # TODO I think this is a bit too much work; let the renderer figure
            # out stuff like levels?
            tokens.append((f"COMMENT{level}", parse(s, level=(level + 1) % 6), span))
        elif typ in {T.LIST, T.NUMBERED_LIST}:
            # Handle the different possible cases. We support arbitrary
            # nesting of lists, multi-line list items and embedded comments.
            tmp: List[Tuple[str, List]] = []
            for l in part.splitlines():
                if not l.strip():
                    continue
                if l.startswith(">"):
                    tmp[-1][1].extend(parse(l))
                elif l.startswith(" "):
                    tmp[-1][1].append((T.PARAGRAPH, parse(l[1:])))
                else:
                    # Should be a line starting with a normal list character (* or 0)
                    tmp.append((T.PARAGRAPH, parse(l[1:].lstrip())))
            tokens.append((typ, [itemize(it, T.LIST_ITEM) for it in tmp], span))

        elif typ == T.DEFINITION_LIST:
            # TODO support lists in desc
            tmp = [((T.TERM, parse_line(l[1:]))
                    if l.startswith("?")
                    else (T.DESC, parse_line(l[1:])))
                   for l in part.splitlines()]
            tokens.append((typ, tmp, span))
        elif typ == T.TABLE:
            # Very basic table support
            tokens.append(
                (T.TABLE, [(T.TABLE_ROW, [(T.TABLE_CELL, parse_line(cell))
                                          for cell in row.split("||")[1:-1]])
                           for row in part.splitlines()], span)
            )
        elif typ == T.VERBATIM:
            tokens.append((typ, part[3:-3], span))
        elif typ == T.NOFORMAT:
            tokens.append((typ, part, span))
        elif typ == T.DISPLAYMATH:
            tokens.append((typ, part[2:-2], span))
        elif typ == T.CITE:
            tokens.append((typ, [(T.PARAGRAPH, parse_line(l))
                                 for l in part.splitlines()], span))
        elif typ == T.HLINE:
            tokens.append((typ, None, span))
        elif typ == T.PARAGRAPH:
            if part.strip():
                tokens.append((typ, parse_line(part), span))
        else:
            tokens.append((typ, part, span))
    return tokens


def check_page_name(name):
    return re.match(WIKI_WORD, name)
