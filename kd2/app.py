from itertools import chain
import logging
from time import time
from traceback import format_exc

from starlette.applications import Starlette
from starlette.config import Config
from starlette.responses import PlainTextResponse, HTMLResponse, RedirectResponse
from starlette.routing import Route, Mount, WebSocketRoute
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

from .wiki import WikiPages, EditConflictError
from .parse import check_page_name


config = Config(".env")

templates = Jinja2Templates(directory='templates')

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s: %(message)s')
handler.setFormatter(formatter)
logger.handlers = []
logger.addHandler(handler)
logger.propagate = False

wiki = WikiPages(config("PAGES_DIR"))


async def show_page(request):
    page_name = request.path_params["page_name"]
    version = int(request.query_params.get("version", -1))
    if "diff" in request.query_params:
        diff_version = int(request.query_params.get("diff"))
    else:
        diff_version = None
    t0 = time()
    try:
        old_name, body_html, body_raw, modtime, commit = wiki.render_page(page_name, request.query_params,
                                                                          version=version,
                                                                          diff_version=diff_version)
        try:
            # Render the header links
            args = {**request.query_params, "page": page_name}  # override page name
            header = wiki.quick_render_page("HeaderLinks", args)
        except IOError:
            header = "Create <a href='/edit/HeaderLinks'>HeaderLinks</a>"
        first_page, prev_page, next_page, last_page = wiki.get_relative_pages(page_name)

        data = dict(name=page_name, old_name=old_name, header=header, body=body_html,
                    first_page=first_page, prev_page=prev_page, next_page=next_page, last_page=last_page,
                    raw_body=body_raw, version=version,
                    render_time=format(time() - t0, ".6f"),
                    last_change=modtime, commit=commit)
        return templates.TemplateResponse("show.html", {"request": request, "config": config, **data})
    except FileNotFoundError:
        return HTMLResponse(f"The page '{page_name}' does not exist! Do you want to <a href='/edit/{page_name}'>create it?</a>")
    except Exception:
        logging.error("Error rendering page: %s", format_exc())
        # TODO Be more specific!
        # return abort(404)
        return PlainTextResponse("404")


async def edit_page(request):
    page_name = request.path_params["page_name"]
    try:
        _, body, _, commit = wiki.load_page(page_name)
        return templates.TemplateResponse("edit.html", dict(request=request, config=config, 
                                                            page_name=page_name, body=body, info="", commit=commit,))
    except FileNotFoundError:
        body = ""
        return templates.TemplateResponse("edit.html", dict(request=request, config=config,
                                                            page_name=page_name, body=body, info="(NY!)"))


async def raw_page(request):
    page_name = request.path_params["page_name"]
    body = wiki.load_page(page_name)[0]
    return PlainTextResponse(body)


async def submit_page(request):
    page_name = request.path_params["page_name"]
    form = await request.form()

    new_name = form.get("name")
    commit = form["commit"]
    minor = form.getlist("minor")

    new_parts = form.getlist("body")
    starts = form.getlist("start")
    ends = form.getlist("end")

    if new_name and not check_page_name(new_name):
        return PlainTextResponse(f"Sorry, '{new_name}' is not a valid wiki name!"
                                 " Your changes were not applied, go back to try again.")

    print("minor", minor)

    if starts and ends:
        # Paragraph(s) edited, let's interleave the new parts with the original
        _, orig_body, _, commit2 = wiki.load_page(page_name)
        logging.debug(f"Edited paragraphs on page: {page_name}, parts: {new_parts}, starts: {starts}, ends: {ends}")
        # Ensure that we're patching the right version
        version = -2
        while not commit2.startswith(commit):
            _, orig_body, _, commit2 = wiki.load_page(page_name, version)
            version -= 1

        indices = [0, *chain.from_iterable(zip(map(int, starts), map(int, ends))), None]
        orig_slices = (slice(a, b) for a, b in zip(indices[::2], indices[1::2]))
        body = ""
        for i, s in enumerate(orig_slices):
            body += orig_body[s]
            if i < len(new_parts):
                body += new_parts[i]
    else:
        # Create or replace page
        body = new_parts[0]
    
    try:
        wiki.save_page(page_name, body, commit, bool(minor),
                       new_name=new_name if new_name != page_name else None)
    except EditConflictError as e:
        # Merge conflict. This should be a really unusual case; One person (A) saves a page that
        # someone else (B) is editing, and then B saves. AND they edited the same line, or otherwise
        # did some change that could not automatically be merged by git. I think it's OK that
        # the user needs to resolve a fairly unintuitive git conflict file in this case.
        return templates.TemplateResponse("conflict.html", dict(request=request,
                                                                config=config,
                                                                name=page_name,
                                                                body=str(e),
                                                                commit=commit))
    except RuntimeError as e:
        return PlainTextResponse(str(e))
   
    return RedirectResponse(url=f"/show/{new_name or page_name}?version=-1&diff=-2#edit")


async def preview_page(request):
    page = request.path_params["page_name"]
    form = await request.form()
    body = form["body"].replace("\r", "")  # Form data uses \r\n as line breaks
    t0 = time()
    body_html = wiki.render_text(page, body, request.query_params)
    data = dict(name=page, header="dsadsaa", body=body_html, version=0, raw_body=body,
                render_time=format(time() - t0, ".6f"))
    return templates.TemplateResponse("preview.html", {"request": request, "config": config, **data})


async def home(request):
    # TODO should be config
    return RedirectResponse(url=f"/show/{config('HOME_PAGE')}")


app = Starlette(routes=[
    Route('/show/{page_name}', show_page, methods=["GET", "POST"], name="show"),
    Route('/edit/{page_name}', edit_page, name="edit"),
    Route('/raw/{page_name}', raw_page, name="raw"),
    Route('/submit/{page_name}', submit_page, methods=["POST"], name="submit"),
    Route('/preview/{page_name}', preview_page, methods=["POST"], name="preview"),
    Route('/', home),

    Mount('/static', app=StaticFiles(directory='static'), name="static")
])

