from collections import Counter
from contextlib import contextmanager
from datetime import datetime
import logging
import os
from pathlib import Path
import re
import subprocess


class EditConflictError(Exception):
    pass


class GitRepo:

    """
    Minimal git wrapper that only does what we need. For now only a local repo.

    TODO
    - using page names as filenames could have problems, maybe "slugify" them?
    - Use asyncio to handle processes, to make things async. Git can be slow.
    - look into pygit2, which uses libgit directly
    """

    LOG_ENTRY_RE = "\n".join([r"commit (?P<hash>.*?)",
                              r"Author: \s*(?P<author>.*?)",
                              r"Date: \s*(?P<timestamp>.*?)",
                              r"",
                              r"(?P<comment>(\s\s\s\s.*?$)+)",
                              r"(\nNotes:\n(?P<notes>(\s\s\s\s.*?$)+)\n+)?",
                              r"(?P<filename>([^\n].+?$))+"])

    def __init__(self, path):
        logging.info("Initializing (if needed) git repo in %r", path)
        self.path = Path(path)
        self.git("init")  # This is always safe, according to git documentation
        self.git("config", "core.quotepath", "off")  # Fix utf8 filename output
        status = self.git("status")
        if not status.startswith("On branch master"):
            try:
                self.git("checkout", "master")
            except RuntimeError as e:
                # This can fail if there have not yet been any commits on master, let's allow it.
                logging.info("Error checking out master; probably the repo was newly created? %s", e)

    def git(self, *command):
        "Run a git command. If all went well, returns the stdout, otherwise raises exception."
        logging.debug("git: %s", " ".join(command))
        process = subprocess.run(["git", *command],
                                 cwd=self.path,
                                 universal_newlines=True,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if process.returncode == 0:
            return process.stdout
        logging.debug("git: %s", process.stderr)
        raise RuntimeError(process.stderr)
    
    @contextmanager
    def _branch(self, page_name, commit_hash):
        "Temporarily check out a branch from the given hash."
        branch = f"{page_name}-{commit_hash}"
        try:
            self.git("checkout", "-b", branch, commit_hash)
        except RuntimeError:
            # Branch presumably exists, guess there's an ongoing conflict.
            self.git("checkout", branch)
        try:
            yield branch
        finally:
            # Ensure that we're always back on master
            self.git("checkout", "master")

    def store(self, page_name, body, commit_hash, minor=False, message="No message"):
        """
        Save a new page or a new revision of an existing page.
        The commit_hash is expected to match the current commit hash of the page,
        otherwise it's considered a conflict that must be resolved.
        """
        # TODO handle the case where no change has been made.
        logging.debug("Storing page %r: %s", page_name, commit_hash)
        try:
            self.git("checkout", "master")
        except RuntimeError:
            pass
        try:
            current_commit_hash = self.git("rev-parse", "--short", "HEAD").strip()
        except RuntimeError:
            # Looks like this is the first commit ever!
            logging.info(f"Adding initial page {page_name}!")
            current_commit_hash = None

        if not commit_hash or not current_commit_hash:
            # New page
            logging.info("Creating page %r", page_name)
            with open(self.path / page_name, "w", newline=None) as f:
                f.write(body)
            self.git("add", page_name)
            self.git("commit", "-m", message)
            self.git("notes", "add", "-m", "NEW")
            # TODO Looks like we can't add more than one note, should we append to it instead?
            # if minor:
            #     self.git("notes", "add", "-m", "MINOR")
            
        elif commit_hash == current_commit_hash:
            # The simple case; nothing happened to the page since the user started edit
            logging.info("Modifying page %r: %s", page_name, commit_hash)
            with open(self.path / page_name, "w", newline=None) as f:
                f.write(body)
            self.git("add", page_name)
            if not self.git("diff", "--staged"):
                # Apparently no changes were made; nothing to do!
                return
            self.git("commit", "-m", message)
            if minor:
                self.git("notes", "add", "-m", "MINOR")
                
        else:
            # Edit conflict; let's try to solve it
            logging.warning("Edit conflict on page %r: %s", page_name, commit_hash)
            with self._branch(page_name, commit_hash) as branch:
                with open(self.path / page_name, "w", newline=None) as f:
                    f.write(body)
                if not self.git("diff"):
                    return
                self.git("add", page_name)
                self.git("commit", "-m", message)
                if minor:
                    self.git("notes", "add", "-m", "MINOR")
                try:
                    # TODO maybe there's a simpler way, but this seems to work:
                    # Merge master to combine with the other changes
                    self.git("merge", "--no-ff", "master")
                    logging.info("Merged edits on page %r: %s", page_name, commit_hash)
                except RuntimeError:
                    # There was presumably a conflict, let the user solve it.
                    logging.warning("Edit conflict on page %r: %s", page_name, commit_hash)
                    with open(self.path / page_name) as f:
                        merge_body = f.read()
                    # Just got to clean up
                    self.git("reset", "--hard", commit_hash)
                    self.git("merge", "master")
                    raise EditConflictError(merge_body)
                # Rebase the branch so that our commit comes last
                self.git("rebase", "master")
            self.git("merge", branch)
            self.git("branch", "-d", branch)

    def rename(self, old_page_name, new_page_name, body, commit_hash):
        logging.info(f"Renaming page '{old_page_name}' to '{new_page_name}'")

        # Check that the new name isn't already taken.
        try:
            self.retrieve(new_page_name)
        except FileNotFoundError:
            pass
        else:
            raise RuntimeError(f"Can't rename page '{old_page_name}'; page '{new_page_name}' already exists! "
                               "Your changes were not applied, go back and try again.")
        
        self.git("mv", old_page_name, new_page_name)
        self.store(new_page_name, body, commit_hash)
        referring_pages = self.git("grep", '--files-with-matches', old_page_name).splitlines()
        for referring_page in referring_pages:
            logging.info("Updating reference to '{page_name}' in page '{referring_page}' to '{new_page_name}'")
            sed_process = subprocess.Popen(["sed", "-i", f"s/{old_page_name}/{new_page_name}/g", referring_page],
                                           cwd=self.path, stdout=subprocess.PIPE)
            sed_process.wait()
            self.git("add", referring_page)
        self.git("commit", "--amend", "--no-edit")
        self.git("notes", "add", "-f", "-m", f"RENAME {old_page_name} -> {new_page_name}")
        
    def retrieve(self, page_name, version=-1):
        "Get the raw text of a page. Also returns the last modification time and the commit hash."
        # Note that the page may have been renamed. page_name is expected to be the current name, but
        # if the page was renamed between then and now, we return the old name.
        version = version if version is not None else -1
        if version == -1:
            # We can skip calling git if we just want the latest version (fast).
            page_path = str(self.path / page_name)
            mod_time = datetime.fromtimestamp(os.path.getmtime(page_path))
            # TODO just this call takes as much time as all the rest of the work to render the page :(
            # Timing the call in a shell is much faster, is there some overhead in check_output?
            try:
                commit_hash = self.git("rev-parse", "--short", "HEAD").strip()
            except RuntimeError:
                # This should mean there are no commits yet. Let's treat it as a missing page
                # since there aren't any pages yet.
                raise FileNotFoundError
            with open(page_path) as f:
                return page_name, f.read(), mod_time, commit_hash
        # To get historical versions we need to look into the git log (slow).
        versions = list(self.versions(page_name, n=-version))
        try:
            page_name, commit_hash, version_time = versions[version]
        except IndexError:
            page_name, commit_hash, version_time = versions[0]
        page_body = self.git("show", f"{commit_hash}:{page_name}")
        return page_name, page_body, version_time, commit_hash

    def references(self, page_name):
        return self.git("grep", '--files-with-matches', page_name).splitlines()
    
    def versions(self, page_name, n):
        "Get n latest log lines"
        log = self.git("log",
                       "-n", str(n),
                       '--name-only', '--date-order',
                       "--date=iso",
                       "--follow",  # Keep track of renamed pages
                       page_name)
        lines = [(m["filename"], m["hash"], datetime.strptime(m["timestamp"], "%Y-%m-%d %H:%M:%S %z"))
                 for m in re.finditer(self.LOG_ENTRY_RE, log, re.MULTILINE)]
        return reversed(lines)

    def _enumerate_page_versions(self, log_entries):
        versions = Counter()
        for e in log_entries:
            yield -versions[e["filename"]] - 1, e
            versions.update([e["filename"]])

    def _cleanup_commit_info(self, info):
        if info.get("notes"):
            info["notes"] = [l.strip() for l in info["notes"].splitlines()]
        else:
            info["notes"] = []
        return info
            
    def history(self, page_name=None, n=37):
        if page_name:
            log = self.git(
                "log",
                "-n", str(n),
                "--date=iso", '--name-only', '--date-order',
                "--follow",
                page_name)
            entries = [self._cleanup_commit_info(m.groupdict())
                       for m in re.finditer(self.LOG_ENTRY_RE, log, re.MULTILINE)]
            return zip(range(-1, -len(entries) - 1, -1), entries)

        else:
            log = self.git(
                "log",
                "-n", str(n),
                "--date=iso", '--name-only', '--date-order')
        
            return self._enumerate_page_versions(
                self._cleanup_commit_info(m.groupdict())
                for m in re.finditer(self.LOG_ENTRY_RE, log, re.MULTILINE))

    def search_page_names(self, term: str):
        """
        Return page names that match the search term.
        """
        # There are simpler ways to do this, but the old wiki used grep and I
        # don't want to lose features... at least we should support regexes.
        ls_proc = subprocess.Popen(["ls"], stdout=subprocess.PIPE, cwd=self.path, universal_newlines=True)
        grep_proc = subprocess.Popen(["grep", "-i", term], stdin=ls_proc.stdout, stdout=subprocess.PIPE)
        ls_proc.stdout.close()
        results, err = grep_proc.communicate()
        return set(results.decode().splitlines())

    def search_in_pages(self, term: str):
        """
        Look for the occurrence of a term in all pages, return the list of
        matching pages.  The term may be an extended grep regex.
        """

        try:
            output = self.git('grep',
                              '--extended-regexp',
                              '--ignore-case',
                              '--files-with-matches',
                              term)
        except RuntimeError as e:
            # Grep did not find anything
            # TODO could be some real error!
            return []

        # Pages were found
        results = output.splitlines()
        logging.debug("Searched pages for %r, got %s", term, ", ".join(results))
        return set(results)

    def list_pages(self, regex):
        find_process = subprocess.Popen(["find", ".", "-regex", f"./{regex}"],
                                        cwd=self.path, stdout=subprocess.PIPE)
        sort_process = subprocess.Popen(["sort", "-V"],
                                        stdin=find_process.stdout, stdout=subprocess.PIPE)
        find_process.wait()
        return [path.decode()[2:].strip() for path in sort_process.stdout]
