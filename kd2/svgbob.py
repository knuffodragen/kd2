import subprocess
from urllib.parse import quote


CACHE = {}  # Runtime cache to save some re-rendering


def render(ascii_diagram: str) -> str:
    """Render an ascii diagram as a HTML image"""
    name = hash(ascii_diagram)
    if name in CACHE:
        return CACHE[name]
    try:
        process = subprocess.run(["svgbob_cli"], input=ascii_diagram.encode(),
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except FileNotFoundError:
        raise RuntimeError("Svgbob not found!")
    if process.returncode != 0:
        raise RuntimeError(f"Svgbob error! {process.stderr.decode()}")
    encoded = "data:image/svg+xml," + quote(process.stdout.decode())
    image = CACHE[name] = f'<img src="{encoded}">'
    return image
