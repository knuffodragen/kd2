"""
Handling of storage, retrieval and rendering of wiki pages.
"""

from datetime import datetime
import html
import logging
from os.path import exists, join
import re
from string import digits
from typing import Tuple, List, Union, Optional, Dict
import urllib.parse

import jinja2
from lxml.html.diff import tokenize, htmldiff_tokens, fixup_ins_del_tags

from .git import GitRepo, EditConflictError
from . import graphviz
from . import svgbob
from .parse import parse, TokenList, T, WIKI_WORD


def replace(text: str, args: Dict[str, str]={}):
    "Return a 'builder' function that formats the given text snippet with arguments."

    def inner(self, page_name, **kwargs):
        return text.format(**args, **kwargs)

    return inner


template_loader = jinja2.FileSystemLoader(searchpath="templates")
template_env = jinja2.Environment(loader=template_loader)

COMMENT_COLORS = ["a00000", "705000", "008000", "006080", "0000a0", "700070"]


def htmldiff(old_html: str, new_html: str):
    """
    Slightly customized version of lxml's htmldiff; added the "include_hrefs=False"
    arguments to prevent link hrefs from being shown (which is redundant here since
    links are always obvious from the link text).
    """
    old_html_tokens = tokenize(old_html, include_hrefs=False)
    new_html_tokens = tokenize(new_html, include_hrefs=False)
    result = htmldiff_tokens(old_html_tokens, new_html_tokens)
    result = ''.join(result).strip()
    return fixup_ins_del_tags(result)


class WikiPages:

    """
    This class is the interface to all wiki pages.

    Here we're mostly concerned with rendering pages to HTML.
    Storage is handled by a git repo.

    TODO:
    - handle errors
    """

    def __init__(self, storage_dir: str):
        self.storage_dir = storage_dir  # main directory
        self.repo = GitRepo(self.storage_dir)

    def get_page_path(self, page_name: str):
        return join(self.storage_dir, page_name)

    def page_exists(self, page_name: str):
        return exists(self.get_page_path(page_name))

    def load_page(self, page_name: str, version=-1) -> Tuple[str, str, datetime, str]:
        "Returns raw text of the given page"
        # TODO handle oldest available version
        return self.repo.retrieve(page_name, version)

    def save_page(self, page_name: str, body: str, commit_hash: str, minor=False, new_name=None):
        "Save the body text as the given page."
        body = body.replace("\r", "")  # Remove garbage from form data
        if new_name:
            self.repo.rename(page_name, new_name, body, commit_hash)
        else:
            self.repo.store(page_name, body, commit_hash, minor)

    def render_page(self, page_name: str, args={}, version=-1, diff_version: Optional[int]=None):
        """
        Retrieve given page from repo and render as HTML. Optionally takes an old version
        and can also include a 'diff' against any other version, to show what changed.
        """
        old_name, body, modtime, commit = self.load_page(page_name, version=version)
        rendered_html = self.build_html(page_name, parse(html.unescape(body)), args, toplevel=True)

        if diff_version is not None:
            other_name, other_body, _, _ = self.load_page(page_name, version=diff_version)
            other_html = self.build_html(other_name, parse(html.unescape(other_body)), args, toplevel=True)
            rendered_html = htmldiff(other_html, rendered_html)
            # Put in an anchor so that the page can automatically be scrolled to the first change
            rendered_html = rendered_html.replace("<ins>", '<a class="edit-anchor" name="edit"></a><ins>')
            rendered_html = rendered_html.replace("<del>", '<a class="edit-anchor" name="edit"></a><del>')
        return old_name, rendered_html, body, modtime, commit
            
    def get_relative_pages(self, page_name):
        "In case there is a series of numbered pages, return relative page links."
        bare_name = page_name.rstrip(digits)

        numbered_pages = self.repo.list_pages(rf"{bare_name}[0-9]*")

        if len(numbered_pages) == 1:
            return None, None, None, None
        
        first_page = numbered_pages[0] if numbered_pages[0] != page_name else None
        index = numbered_pages.index(page_name)
        prev_page = numbered_pages[index - 1] if index > 0 else None
        next_page = numbered_pages[index + 1] if index < len(numbered_pages) - 1 else None
        last_page = numbered_pages[-1] if numbered_pages[-1] != page_name else None

        return first_page, prev_page, next_page, last_page

    def quick_render_page(self, page_name: str, args: Dict[str, str] = {}):
        "Render function that simply renders the latest version."
        _, body, _, _ = self.load_page(page_name)
        return self.build_html(page_name, parse(html.unescape(body)), args, toplevel=True)

    def render_text(self, page_name: str, body: str, args: Dict[str, str] = {}):
        "Just render the given wiki syntax string as HTML"
        return self.build_html(page_name, parse(html.unescape(body)), args, toplevel=True)

    def make_page_link(self, page_name, content: str, args=None, version=None, diff_version=None, **kwargs):
        to_page_url = urllib.parse.quote(content)
        if self.page_exists(content):
            if version is not None:
                diff_version = diff_version if diff_version is not None else version - 1
                return f'<a href="/show/{to_page_url}?version={version}&diff={diff_version}#edit">{content}</a>'
            else:
                return f'<a href="/show/{to_page_url}">{content}</a>'
        else:

            return f'{content}<a href="/edit/{to_page_url}">?</a>'

    # TODO This is pretty much the original syntax for commands. It's not great,
    # maybe time to change it? Commands haven't been used too much anyway...
    COMMANDS = "|".join([
        "(?P<search>search=(?P<pagename>.+))",
        "(?P<wordsearch>wordsearch(=(?P<wordterm>.+))?)",
        "(?P<namesearch>namesearch(=(?P<nameterm>.+))?)",
        "(?P<changes>changes(=(?P<n_changes>\d+))?)",
        "(?P<historylink>historylink)",
        "(?P<history>history(=(?P<n_history>\d+))?)",
        "(?P<olderlink>olderlink)",
        "(?P<newerlink>newerlink)",
        "(?P<latestlink>latestlink)",
        "(?P<referenceslink>referenceslink)",
        "(?P<link>link)",
        "(?P<edit>edit)",
        "(?P<image>image=(?P<image_url>.*))",
    ])

    def perform_command(self, page_name: str, content: str, args: Dict[str, str], **kwargs):

        "Commands are special instructions to the wiki, e.g. to perform a grep and insert the results."

        match = re.match(self.COMMANDS, content)

        if match:
            if match.group("wordsearch"):
                # If we get an argument, we search for it directly.
                # Otherwise present a search form.
                wordterm = match.group("wordterm")
                if wordterm:
                    pages = self.repo.search_in_pages(wordterm)
                    logging.info("Looked for %r in %r, found %r pages.",
                                 wordterm, self.storage_dir, len(pages))
                    if pages:
                        pages.discard(page_name)
                        results = ", ".join(self.make_page_link(page_name, other)
                                            for other in sorted(pages))
                        results = f'<div class="search-results">{results}</div>'
                    else:
                        results = ""
                    return results
                else:
                    # This command takes the search term from the page query parameter "search".
                    wordterm = args.get("search", "")
                    if wordterm:
                        pages = self.repo.search_in_pages(wordterm)
                        logging.info("Looked for %r in %r, found %r pages.", wordterm, self.storage_dir, len(pages))
                        return ('<div class="search-results">' +
                                ", ".join(self.make_page_link(page_name, p) for p in sorted(pages)) +
                                '</div>')
                    else:
                        return ""

            elif match.group("namesearch"):
                # Works like wordsearch but looks in the names of pages instead.
                nameterm = match.group("nameterm")
                if nameterm:
                    pages = self.repo.search_page_names(nameterm)
                    logging.info("Looked for %r in %r, found %r pages.",
                                 nameterm, self.storage_dir, len(pages))
                    if pages:
                        pages.discard(page_name)
                        results = ", ".join(self.make_page_link(page_name, other)
                                            for other in sorted(pages))
                        results = f'<div class="search-results">{results}</div>'
                    else:
                        results = ""  # TODO formatting?
                    return results
                else:
                    # This command takes the search term from the page query parameter "search".
                    nameterm = args.get("search", "")
                    if nameterm:
                        pages = self.repo.search_page_names(nameterm)
                        logging.info("Looked for %r in %r, found %r pages.", nameterm, self.storage_dir, len(pages))
                        return ('<div class="search-results">' +
                                ", ".join(self.make_page_link(page_name, p) for p in pages) +
                                '</div>')
                    else:
                        return ""

            elif match.group("changes"):
                n = match.group("n_changes")
                changes = self.repo.history(n=(int(n) if n else 37))

                # Try to collect the edits into one per page, in a reasonably logical way.
                changes_by_page = {}
                for v, c in changes:
                    name = c["filename"]
                    changes_by_page.setdefault(name, []).append((v, c))

                combined = []
                RENAME_PATTERN = f"RENAME (?P<old_name>{WIKI_WORD}) -> (?P<new_name>{WIKI_WORD})"
                for name, cs in changes_by_page.items():

                    # If all changes to the page are noted as "minor", let's not show it.
                    minor = all("MINOR" in c["notes"] for _, c in cs)                    
                    if minor:
                        continue
                    old_name = None

                    # A "rename" commit contains both the renaming of the page, any edits, plus
                    # updated links from other pages. This means that the list of changed files can
                    # be long, and we can't just use the first one. Instead we rely on the presence
                    # of a "note" that has the info we need.
                    for _, c in cs:
                        if not c["notes"]:
                            continue
                        note = c["notes"][0]
                        match = re.match(RENAME_PATTERN, note)
                        if match:
                            old_name = match.group("old_name")
                            name = match.group("new_name")

                    new = any("NEW" in c["notes"] for _, c in cs)
                    
                    latest_version, latest_data = cs[0]
                    version = latest_version if not new else None
                    oldest_version, oldest_data = cs[-1]
                    diff_version = oldest_version - 1 if len(cs) > 1 else None
                    timestamp = latest_data["timestamp"]
                    combined.append(dict(name=name, old_name=old_name,
                                         timestamp=timestamp,
                                         version=version,
                                         diff_version=diff_version,
                                         new=new))

                template = template_env.get_template("changes.html")
                return template.render(make_page_link=self.make_page_link, changes=combined)

            elif match.group("history"):
                page = args.get("page", page_name)
                n = match.group("n_history")
                changes = self.repo.history(page_name=page, n=(int(n) if n else 37))
                template = template_env.get_template("page_history.html")
                return template.render(make_page_link=self.make_page_link, page=page, changes=changes)

            elif match.group("image"):
                url = match.group("image_url")
                return f'<img src="{url}" />'

            elif match.group("search"):
                search_page_name = match.group("pagename") or "SökSidan"
                search = args.get("search") or ""
                return (
                    '<div style="display: inline-block;">'
                    + 'Search: <form style="margin:0;padding:0;display:inline-block;" '
                    + f'action="/show/{search_page_name}" method="GET">'
                    + f'<input name="search" type="search" value="{search}"/></form></div>'
                )

            elif match.group("historylink"):
                page = args.get("page", page_name)
                return f'<a href="/show/HistoryPage?page={page}">History</a>'
            elif match.group("olderlink"):
                page = args.get("page", page_name)
                version = int(args.get("version") or -2)
                diff = int(args.get("diff") or -3)
                return f'<a href="/show/{page}?version={version - 1}&diff={diff - 1}#edit">Older</a>'
            elif match.group("newerlink"):
                page = args.get("page", page_name)
                version = int(args.get("version", -1))
                if version == -1:
                    return "Newer"
                else:
                    return f'<a href="/show/{page}?version={version + 1}&diff={version}#edit">Newer</a>'
            elif match.group("latestlink"):
                page = args.get("page", page_name)
                return f'<a href="/show/{page}?version=-1&diff=-2#edit">Latest</a>'

            elif match.group("referenceslink"):
                page = args.get("page", page_name)
                search_page_name = "SökSidan"
                return f'<a href="/show/{search_page_name}?search={page}">Refereces</a>'

            elif match.group("link"):
                page = args.get("page", page_name)
                return f'<a href="/show/{page}">Link</a>'

            elif match.group("edit"):
                page = args.get("page", page_name)
                return f'<a href="/edit/{page}">Edit</a>'

        print("Bad command: %r did not match any known command." % content)
        return "<b>BAD COMMAND</b>"

    COMMAND_BLOCKS = "|".join([
        r"(?P<dot>dot=\n(?P<dotcode>(.*(\n|\Z))+))",
        r"(?P<bob>bob=\n(?P<bobcode>(.*(\n|\Z))+))",
    ])

    def perform_command_block(self, page_name: str, content: str, args: Dict[str, str], **kwargs):
        match = re.match(self.COMMAND_BLOCKS, content, flags=re.MULTILINE)

        if match.group("dot"):
            if not graphviz:
                return "[graphviz support disabled]"
            dot_code = match.group("dotcode")
            try:
                return graphviz.render(dot_code)
            except RuntimeError as e:
                logging.error(f"Bad command: graphviz failed to render! Error: {e}")
                return f"<b>Bad command: {e}</b>"

        elif match.group("bob"):
            bob_code = match.group("bobcode")
            try:
                return svgbob.render(bob_code)
            except RuntimeError as e:
                logging.error(f"Bad command: svgbob failed to render! Error: {e}")
                return f"<b>Bad command: {e}</b>"

        logging.error("Bad command block: %r did not match any known command." % content.splitlines()[0])
        return "<b>BAD COMMAND BLOCK</b>"

    # Instructions on how to render the different kinds of tokens as HTML
    # "Functionality" tokens are those that go further than just styling, and involve
    FUNCTIONALITY_TOKENS = {
        T.WIKI_WORD: make_page_link,
        T.COMMAND: perform_command,
        T.COMMAND_BLOCK: perform_command_block,
        T.URL: replace('<a href="{content}">{content}</a>'
                       ' <a href="https://web.archive.org/web/*/{content}">[A]</a>'),
    }

    STYLE_TOKENS = {
        T.NOFORMAT: replace('<span data-start={start} data-end={end}>{content}</span>'),
        T.MATH: replace('<script type="math/tex">{content}</script>'),
        T.DISPLAYMATH: replace('<div data-start={start} data-end={end}><script type="math/tex;mode=display">{content}</script></div>'),
        T.BOLD: replace("<b>{content}</b>"),
        T.ITALIC: replace("<i>{content}</i>"),
        T.VERBATIM: replace("<pre data-start={start} data-end={end}>{content}</pre>"),
        T.UNDERSCORE: replace("<u>{content}</u>"),
        T.BIGGER: replace('<font size="+1">{content}</font>'),
        T.SMALLER: replace('<font size="-1">{content}</font>'),
        T.SUB: replace('<sub>{content}</sub>'),
        T.SUB2: replace('<sub>{content}</sub>'),
        T.SUP: replace('<sup>{content}</sup>'),
        T.SUP2: replace('<sup>{content}</sup>'),
        T.CITE: replace('<blockquote data-start={start} data-end={end}>{content}</blockquote>'),
        T.CODE: replace('<code data-start={start} data-end={end}>{content}</code>'),
        T.TERM: replace('<dt>{content}</dt>'),
        T.DESC: replace('<dd>{content}</dd>'),

        T.HLINE: replace('<hr>'),
        T.PARAGRAPH: replace('<p data-start={start} data-end={end}>{content}</p>'),

        **{
            # f"COMMENT{i}": replace(f'<div style="padding-left: 20px; color: #{COMMENT_COLORS[i]}">'
            #                        '{content}</div>')
            f"COMMENT{i}": replace(f'<div style="padding-left: 20px; color: #{COMMENT_COLORS[i]}" data-start={{start}} data-end={{end}}>'
                                   '{content}</div>')
            for i in range(len(COMMENT_COLORS))
        },
        T.LIST: replace('<ul data-start={start} data-end={end}>{content}</ul>'),
        T.NUMBERED_LIST: replace('<ol data-start={start} data-end={end}>{content}</ol>'),
        T.LIST_ITEM: replace('<li>{content}</li>'),
        T.DEFINITION_LIST: replace('<dl data-start={start} data-end={end}>{content}</dl>'),
        T.TABLE: replace('<table class="wiki" data-start={start} data-end={end}>{content}</table>'),
        T.TABLE_ROW: replace('<tr>{content}</tr>'),
        T.TABLE_CELL: replace('<td>{content}</td>'),
    }

    def build_html(self, page_name: str, tree: TokenList, args: Dict[str, str]={}, toplevel=False):
        "Takes a parsed tree and outputs a HTML string."
        parts = []

        TOKEN_TO_HTML = {**self.STYLE_TOKENS, **self.FUNCTIONALITY_TOKENS}

        if isinstance(tree, str):
            return tree

        for token in tree:
            if isinstance(token, str):
                # Leaf node
                parts.append(html.escape(token))
            elif isinstance(token, tuple):
                typ, content, *span = token

                if toplevel:
                    # Provide start and end positions of the orignal markup for this element
                    start, end = span[0]
                else:
                    start = end = -1

                if typ in TOKEN_TO_HTML:
                    builder = TOKEN_TO_HTML[typ]
                    if content is not None:
                        # Recurse into the subtree
                        parts.append(builder(self, page_name,
                                             content=self.build_html(page_name, content, args),
                                             start=start, end=end, args=args))
                    else:
                        parts.append(builder(self, page_name, args=args))
                else:
                    pass
        return ''.join(parts)
