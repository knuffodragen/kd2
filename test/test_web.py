"""
Some "end-to-end" tests that check basic functionality using a fake browser.
"""

import mechanize


HOME_PAGE = "TestHomePage"
HOME_PAGE_BODY = "This is some text."


def create_page(browser, wiki, name, body, preview=False):
    browser.open(f"{wiki}edit/{name}")
    browser.select_form(id="page")
    browser["body"] = body
    if preview:
        browser.submit(id="preview")
    else:
        try:
            browser.submit()
        except Exception as e:  # mechanize._response.get_seek_wrapper_class.<locals>.httperror_seek_wrapper
            location, = e.headers.getheaders("location")
        return location


def append_to_page(browser, wiki, name, appendage, preview=False):
    browser.open(f"{wiki}edit/{name}")
    browser.select_form(id="page")
    browser["body"] = browser["body"] + appendage
    if preview:
        browser.submit(id="preview")
    else:
        try:
            browser.submit()
        except Exception as e:  # mechanize._response.get_seek_wrapper_class.<locals>.httperror_seek_wrapper
            print(e)
            location, = e.headers.getheaders("location")
        return location


def check_body(page, expected_body):
    tree = mechanize._html.content_parser(page)
    paragraph, = tree.findall(".//article/p")
    assert paragraph.text == expected_body
    

# ============== Tests ==============

def test_empty_repo(empty_wiki, browser):
    # Access the empty wiki
    browser.open(empty_wiki)
    # There is no home page, but we should get a link to create it
    assert browser.find_link(url_regex=f"edit/{HOME_PAGE}")


def test_create_first_page(empty_wiki, browser):
    # Create a page in an empty wiki
    create_page(browser, empty_wiki, HOME_PAGE, HOME_PAGE_BODY)
    browser.open(empty_wiki)
    check_body(browser.response(), HOME_PAGE_BODY)


def test_get_home_page(wiki, browser):
    # Get the default ("home") page
    browser.open(wiki)
    check_body(browser.response(), HOME_PAGE_BODY)


def test_edit_page(wiki, browser):
    # Edit an existing page
    NEW_STUFF = " Some extra stuff."
    append_to_page(browser, wiki, HOME_PAGE, NEW_STUFF)
    browser.open(wiki)
    check_body(browser.response(), HOME_PAGE_BODY + NEW_STUFF)

    
def test_edit_page_redirects_to_show(wiki, browser):
    # Edit an existing page
    NEW_STUFF = " Some extra stuff."
    location = append_to_page(browser, wiki, HOME_PAGE, NEW_STUFF)
    assert location.startswith(f"/show/{HOME_PAGE}")

    
def test_edit_preview(wiki, browser):
    # Edit an existing page
    NEW_STUFF = " Some extra stuff."
    append_to_page(browser, wiki, HOME_PAGE, NEW_STUFF, preview=True)

    # Check preview
    check_body(browser.response(), HOME_PAGE_BODY + NEW_STUFF)

    # Submit edits
    browser.select_form(nr=0)
    try:
        browser.submit()  # TODO No exception?!
    except Exception as e:
        location, = e.headers.getheaders("location")
    browser.open(f"{wiki}show/TestHomePage")
    check_body(browser.response(), HOME_PAGE_BODY + NEW_STUFF)

   
def test_page_versions(wiki, browser):
    # Edit an existing page
    NEW_STUFF = " Some extra stuff."
    append_to_page(browser, wiki, HOME_PAGE, NEW_STUFF)

    # Load diff with previous version
    browser.open(f"{wiki}show/TestHomePage")
    page = browser.follow_link(text="Latest")
    tree = mechanize._html.content_parser(page)
    inserted, = tree.findall(".//article/p/ins")
    assert inserted.text.strip() == NEW_STUFF.strip()


# TODO edit conflict
# TODO page rename
