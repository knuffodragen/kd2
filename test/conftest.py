from contextlib import closing
import os
import socket
import subprocess
import time
from uuid import uuid4

import mechanize
import pytest


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


port = find_free_port()


@pytest.fixture()
def empty_wiki():
    
    """ A brand new, empty wiki. """

    random_path, *_ = str(uuid4()).split("-")
    pages_dir = f"/tmp/kd2-{random_path}"
    os.mkdir(pages_dir)
    test_env = os.environ.copy()
    test_env["PAGES_DIR"] = pages_dir
    test_env["HOME_PAGE"] = "TestHomePage"
    proc = subprocess.Popen(["poetry", "run", "uvicorn", "kd2:app", f"--port={port}"], env=test_env)
    time.sleep(1)
    # TODO make sure the server is actually up
    
    yield f"http://localhost:{port}/"
    
    proc.terminate()


@pytest.fixture()
def wiki():
    
    """ A wiki populated with a minimum of pages. """

    random_path, *_ = str(uuid4()).split("-")
    pages_dir = f"/tmp/kd2-{random_path}"
    os.mkdir(pages_dir)
    test_env = os.environ.copy()
    test_env["PAGES_DIR"] = pages_dir
    test_env["HOME_PAGE"] = "TestHomePage"
    proc = subprocess.Popen(["poetry", "run", "uvicorn", "kd2:app", f"--port={port}"], env=test_env)
    time.sleep(1)
    # TODO make sure the server is actually up

    browser = mechanize.Browser()
    browser.set_handle_robots(False)
    browser.set_handle_redirect(True)  # This does not seem to help; 307 does not work
    PAGE_BODY = "This is some text."
    browser.open(f"http://localhost:{port}/edit/TestHomePage")
    browser.select_form(id="page")
    browser["body"] = PAGE_BODY
    try:
        browser.submit()
    except Exception as e:  # mechanize._response.get_seek_wrapper_class.<locals>.httperror_seek_wrapper
        location, =  e.headers.getheaders("location")
    
    yield f"http://localhost:{port}/"

    proc.terminate()
    

@pytest.fixture
def browser():
    
    """ A fake browser. """
    
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.set_handle_redirect(True)
    return br
