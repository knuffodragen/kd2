import pytest

from kd2.parse import parse, parse_line, T


class AnyInt:

    """
    Something that is equal to any integer.
    Nice for assertions where we don't care what the number is.
    TODO We *should* care, it's just tedious. And in most cases
    the numbers aren't used in the end.
    """

    def __eq__(self, other):
        return isinstance(other, int)


# parse_line() tests (single line)

def test_wiki_word():
    text = "This line contains a WikiWord, and that's the truth."
    tokens = parse(text)
    assert tokens == [
        ("PARAGRAPH", [
            "This line contains a ",
            ("WIKI_WORD", ["WikiWord"]),
            ", and that's the truth."
        ], (0, 52))
    ]


def test_url():
    text = "An URL like http://www.example.com/some/path.html should work."
    tokens = parse_line(text)
    assert tokens == ['An URL like ',
                      (T.URL, ['http://www.example.com/some/path.html']),
                      ' should work.']


def test_command():
    text = "A command like [[this]] ought to work."
    tokens = parse_line(text)
    assert tokens == ['A command like ',
                      (T.COMMAND, ['this']),
                      ' ought to work.']


def test_noformat():
    text = r"Någonting \\men ##utan##\nformattering\\ eftertext"
    tokens = parse_line(text)
    assert tokens == ["Någonting ", (T.NOFORMAT, [r"men ##utan##\nformattering"]), " eftertext"]


def test_math():
    text = r"Hej $x^2$ hå"
    tokens = parse_line(text)
    assert tokens == ["Hej ", (T.MATH, ["x^2"]), " hå"]


def test_math_no_eol():
    text = "He paid $500 for it."
    tokens = parse_line(text)
    assert tokens == ["He paid $500 for it."]


def test_bold():
    text = r"Hej ##fetstil## hå"
    tokens = parse_line(text)
    assert tokens == ["Hej ", (T.BOLD, ["fetstil"]), " hå"]


def test_italic():
    text = r"Hej ''kursiv'' hå"
    tokens = parse_line(text)
    assert tokens == ["Hej ", (T.ITALIC, ["kursiv"]), " hå"]


def test_bold_eol():
    text = r"Hej ##fetstil"
    tokens = parse_line(text)
    assert tokens == ["Hej ", (T.BOLD, ["fetstil"])]


def test_code():
    text = "Blabla ==some code== dobido fdadas"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.CODE, ['some code']), ' dobido fdadas']


def test_sub():
    text = "Blabla ,,sub text,, rest"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.SUB, ['sub text']), ' rest']


def test_sub2():
    text = "Blabla _{sub text} rest"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.SUB2, ['sub text']), ' rest']


def test_sup():
    text = "Blabla ^^super text^^ rest"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.SUP, ['super text']), ' rest']


def test_sup2():
    text = "Blabla ^{super text} rest"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.SUP2, ['super text']), ' rest']


def test_smaller():
    text = "Blabla --smaller text-- rest"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.SMALLER, ['smaller text']), ' rest']


def test_bigger():
    text = "Blabla ++bigger text++ rest"
    tokens = parse_line(text)
    assert tokens == ['Blabla ', (T.BIGGER, ['bigger text']), ' rest']


# parse() tests (multi line)

def test_no_format():
    text = r"This \\is ##some## text ''with''\\ formatting."
    tokens = parse(text)
    assert tokens == [('PARAGRAPH', ["This ",
                                     ("NOFORMAT", ["is ##some## text ''with''"]),
                                     " formatting."], (0, 46))]


def test_verbatim():
    text = r"This ==is ##some## text ''with''== formatting."
    tokens = parse(text)
    assert tokens == [('PARAGRAPH', ["This ",
                                     ("VERBATIM", ["is ##some## text ''with''"]),
                                     " formatting."], (0, 46))]


def test_no_format_multi():
    text = r"""Normal ##text##.
\\
This is ##some## text ''with''
formatting."
\\
Normal again."""
    tokens = parse(text)
    print(tokens)
    assert tokens == [
        ('PARAGRAPH', ['Normal ', ('BOLD', ['text']), '.'], (0, 16)),
        ('NOFORMAT', '\nThis is ##some## text \'\'with\'\'\nformatting."\n', (19, 64)),
        ('PARAGRAPH', ['Normal again.'], (67, 80))
    ]


def test_comment():
    text = """Det här är ett påstående!
>Det här är ett mothugg!
>>Det är det inte alls.
Fortsätter som inget hade hänt."""
    tokens = parse(text)
    print(tokens)
    assert tokens == [
        ('PARAGRAPH', ['Det här är ett påstående!'], (0, 25)),
        ('COMMENT0', [('PARAGRAPH', ['Det här är ett mothugg!'], (0, 23))], (26, 50)),
        ('COMMENT0', [('COMMENT1', [('PARAGRAPH', ['Det är det inte alls.'], (0, 21))], (0, 22))], (51, 74)),
        ('PARAGRAPH', ['Fortsätter som inget hade hänt.'], (75, 106))
    ]


def test_verbatim():
    text = """En gnutta text
==
Verbatim text ##med## en ++del++ ''formattering'' i
==
Annan text här"""
    tokens = parse(text)
    assert tokens == [
        ('PARAGRAPH', ['En gnutta text'], (0, 14)),
        ('VERBATIM', "Verbatim text ##med## en ++del++ ''formattering'' i", (15, 72)),
        ('PARAGRAPH', ['Annan text här'], (73, 87))
    ]


def test_cite():
    text = """Here is a normal line
 This is a quote
    which continues here.
Normal text again"""
    tokens = parse(text)

    assert tokens == [('PARAGRAPH', ['Here is a normal line'], (0, 21)),
                      ('CITE', [
                          ('PARAGRAPH', [' This is a quote']),
                          ('PARAGRAPH', ['    which continues here.'])
                      ], (22, 65)),
                      ('PARAGRAPH', ['Normal text again'], (65, 82))]


def test_list():
    text = """some text before
*List item 1
*List item 2
Some text after"""

    tokens = parse(text)

    assert tokens[0] == ('PARAGRAPH', ['some text before'], (0, 16))
    assert tokens[1] == (
        'LIST', [
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 1"], (AnyInt(), AnyInt()))]),
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 2"], (AnyInt(), AnyInt()))]),
        ], (17, 43)
    )
    assert tokens[2] == ('PARAGRAPH', ["Some text after"], (43, 58))


def test_list_ignores_empty_line():
    text = """some text before
*List item 1

*List item 2
Some text after"""

    tokens = parse(text)

    assert tokens[0] == ('PARAGRAPH', ['some text before'], (0, 16))
    assert tokens[1] == (
        'LIST', [
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 1"], (AnyInt(), AnyInt()))]),
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 2"], (AnyInt(), AnyInt()))]),
        ], (17, 44)
    )
    assert tokens[2] == ('PARAGRAPH', ["Some text after"], (44, 59))


def test_list_containing_comment():
    text = """some text before
*List item 1
>Comment to item 1
*List item 2
Some text after"""

    tokens = parse(text)
    print(tokens)
    assert tokens[0] == ('PARAGRAPH', ['some text before'], (0, 16))
    assert tokens[1] == (
        'LIST',
        [('LIST_ITEM', [('PARAGRAPH', ['List item 1'], (0, 11)),
                        ('COMMENT0', [('PARAGRAPH', ['Comment to item 1'], (0, 17))], (0, 18))]),
         ('LIST_ITEM', [('PARAGRAPH', ['List item 2'], (0, 11))])], (17, 62)
    )
    assert tokens[2] == ('PARAGRAPH', ["Some text after"], (62, 77))


def test_list_with_leading_space():
    text = """some text before
* List item 1
*List item 2
*  List item 3
Some text after"""

    tokens = parse(text, 0)

    assert tokens[0] == ('PARAGRAPH', ['some text before'], (0, 16))
    assert tokens[1] == (
        'LIST', [
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 1"], (AnyInt(), AnyInt()))]),
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 2"], (AnyInt(), AnyInt()))]),
            ('LIST_ITEM', [(T.PARAGRAPH, ["List item 3"], (AnyInt(), AnyInt()))]),
        ],
        (17, 59)
    )
    assert tokens[2] == ('PARAGRAPH', ["Some text after"], (59, 74))


def test_nested_list():
    text = """some text before
*List item 1
 *List item a
  *List item x
 *List item b
*List item 2
Some text after"""

    tokens = parse(text)
    print(tokens)

    assert tokens == [
        ('PARAGRAPH', ['some text before'], (0, 16)),
        ('LIST', [
            ('LIST_ITEM', [
                ('PARAGRAPH', ['List item 1'], (AnyInt(), AnyInt())),
                ('PARAGRAPH', [
                    ('LIST', [
                        ('LIST_ITEM', [('PARAGRAPH', ['List item a'], (AnyInt(), AnyInt()))])
                    ], (AnyInt(), AnyInt()))
                ]),
                ('PARAGRAPH', [
                    ('LIST', [
                        ('LIST_ITEM', [('PARAGRAPH', ['List item x'], (AnyInt(), AnyInt()))])
                    ], (AnyInt(), AnyInt()))
                ]),
                ('PARAGRAPH', [
                    ('LIST', [
                        ('LIST_ITEM', [('PARAGRAPH', ['List item b'], (AnyInt(), AnyInt()))])
                    ], (AnyInt(), AnyInt()))
                ])]),
            ('LIST_ITEM', [('PARAGRAPH', ['List item 2'], (AnyInt(), AnyInt()))])
        ], (17, 86)),
        ('PARAGRAPH', ['Some text after'], (86, 101))
    ]


def test_list_with_multiline_item():

    # Not yet supported

    text = """some text before
* List item 1
  extra line in item 1
  even more
* List item 2
Some text after"""

    tokens = parse(text)
    assert tokens[0] == ('PARAGRAPH', ['some text before'], (0, 16))
    print(tokens[1])
    assert tokens[1] == (
        'LIST',
        [
            ('LIST_ITEM', [
                ('PARAGRAPH', ['List item 1'], (AnyInt(), AnyInt())),
                ('PARAGRAPH', [('PARAGRAPH', ['extra line in item 1'], (AnyInt(), AnyInt()))]),
                ('PARAGRAPH', [('PARAGRAPH', ['even more'], (AnyInt(), AnyInt()))])
            ]),
            ('LIST_ITEM', [('PARAGRAPH', ['List item 2'], (AnyInt(), AnyInt()))])
        ],
        (17, 80))
    assert tokens[2] == ('PARAGRAPH', ["Some text after"], (80, 95))


def test_definition():

    # Not yet supported

    text = """Some text
?A definition
!Some description
Final text"""
    tokens = parse(text)
    print(tokens)
    assert tokens[0] == ("PARAGRAPH", ["Some text"], (0, 9))
    assert tokens[1] == (
        "DEFINITION_LIST", [
            ("TERM", ["A definition"]),
            ("DESC", ["Some description"])
        ], (10, 42))
    assert tokens[2] == ("PARAGRAPH", ["Final text"], (42, 52))


@pytest.mark.xfail
def test_definition_with_list():

    # Not yet supported

    text = """Some text
?A definition
!Some description
 * List element 1
 * List element 2
 Further description
Final text"""
    tokens = parse(text)
    print(tokens)
    assert tokens[0] == ("PARAGRAPH", ["Some text"], (0, 9))
    assert tokens[1] == (
        "DEFINITION_LIST", [
            ("TERM", ["A definition"]),
            ("DESC", ["Some description",
                      ("LIST", [("LIST_ITEM", ["List element 1"]),
                                ("LIST_ITEM", ["List element 2"])]),
                      "Further description"], (10, 99))
        ])
    assert tokens[2] == ("PARAGRAPH", ["Final text"], (AnyInt(), AnyInt()))


def test_displatmath():
    text = r"""Nonmath
$$\sqrt{3}$$
Other"""
    tokens = parse(text)
    assert tokens == [
        ('PARAGRAPH', ['Nonmath'], (0, 7)),
        ('DISPLAYMATH', '\\sqrt{3}', (8, 20)),
        ('PARAGRAPH', ['Other'], (21, 26))
    ]


def test_hline():
    text = """Blabla
----
Blublu"""
    tokens = parse(text)
    assert tokens == [
        ('PARAGRAPH', ['Blabla'], (0, 6)),
        ('HLINE', None, (7, 11)),
        ('PARAGRAPH', ['Blublu'], (12, 18))
    ]


def test_paragraph():
    text = """Just some text.
More text."""
    tokens = parse(text)
    print(tokens)
    assert tokens == [
        ('PARAGRAPH', ['Just some text.'], (0, 15)),
        ('PARAGRAPH', ['More text.'], (16, 26))
    ]


def test_verbatim_with_trailing_space():
    text = "\n".join([
        "Start!",
        "",
        "==      ",  # trailing space here; parser would hang indefinitely
        "    some unformatted stuff",
        "",
        "== ",
        "",
        "Done!"
    ])
    tokens = parse(text)
    assert tokens[1][0] == "VERBATIM"
    assert tokens[1][1] == "     \n    some unformatted stuff\n\n"


def test_verbatim_diagram():
    text = r"""==
                           _____________
                T         |             |       T
   (  )------Element----->|  Rumstemp.  |----Läckage----->(  )
                ^         |_____________|       ^
    Önskad       \          /         \         |
   temperatur     |  B     /           \    B   |
        |         |       v             v       |
        |       Skillnad mellan       Skillnad mellan
         ----> Önskad och uppmätt     Inne- och ute-  <----Ute-temp.
                    temperatur          temperatur.
=="""
    tokens = parse(text)
    assert "".join(tokens[0][1]) == text[3:-3]
