from setuptools import setup


setup(
    name="kd2",
    description="A small wiki",
    version="2.0.0",
    author="Johan Forsberg",
    author_email="johan@slentrian.org",
    url="https://gitlab.com/knuffodragen/kd2",
    packages=['kd2'],
    install_requires=[
        "dateutils",
        "lxml>=4.9.1",  # HDML diff is buggy(er) in other versions
        "starlette",
        "uvicorn",
        "aiofiles",
        "jinja2",
        "python-multipart",
        "MarkupSafe == 2.0.1",  # Temporary fix https://github.com/dbt-labs/dbt-core/issues/4745
    ]
)
