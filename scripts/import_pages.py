"""
This is a single purpose script for importing pages from an old version of the
wiki. It should be removed at some future point when the import has been verified
to be good enough.

Known issues:
- The import does not work all the way back to to the start of history for the
oldest pages. This is probably due to some stupid format change in the diff files
around 2003. It seems the history function in the old wiki does not really work
that far back either so it's probably hopeless to get it to work properly.
"""

from datetime import datetime
import os
import re
import shutil
import subprocess
from tempfile import TemporaryDirectory
from typing import List, Tuple

from dateutil import parser
import pytz

from kd2.parse import WIKI_WORD


def get_versions(page_name, page_text: str, diffs: str, tmpdir):

    patches = diffs.split("***\n")

    # Here, the version goes backwards; version 0 is the latest version,
    # 1 the one before, etc
    version = 0

    # Write the current version
    with open(os.path.join(tmpdir, f"{page_name}-{version:03d}"), "w") as f:
        f.write(page_text)
    # Go through patches and apply each to the current version
    # to get the next version. Patches are stored chronologically so we
    # must go through them in reverse order.

    try:
        date, ip = patches[-1].splitlines()[:2]
    except ValueError:
        # Last one seems empty, skip it
        patches = patches[:-1]
        date, ip = patches[-1].splitlines()[:2]

    versions = []
    for i, patch in enumerate(reversed(patches)):
        # We must go backwards from the latest version and apply the patches
        # incrementally to get all versions of the page.
        if not patch.strip():
            continue
        date, ip, *patchlines = patch.splitlines()
        patch = "\n".join(patchlines) + "\n"
        infile = f"{page_name}-{version:03d}"
        outfile = f"{page_name}-{version+1:03d}"

        p = subprocess.run([
            "patch",
            "-l",  # Ignore whitespace
            "-f",  # Don't ask
            # "--verbose",
            os.path.join(tmpdir, infile),
            "-o", os.path.join(tmpdir, outfile)
        ], input=patch.encode("utf-8"))

        if p.returncode != 0:
            # raise RuntimeError(f"Patch {version} (of {len(patches)}) failed; {p.stdout}.")
            continue
        versions.append((date, ip, infile))
        version += 1

    for date, ip, filename in reversed(versions):
        if date:
            timestamp = parser.parse(date.replace("MEST", "CEST"))  # Obsolete abbrev
            try:
                timestamp = pytz.utc.localize(timestamp)
            except ValueError:
                pass
        else:
            timestamp = None
        yield timestamp, page_name, os.path.join(tmpdir, filename)


GIT_DATE_FMT = "%Y-%d-%m %H:%M:%S %z"
GIT_DATE_FMT2 = "%a %b %d %b %H:%M:%S %Y %z"
# Fri Jul 26 19:32:10 2013 -0400'


def read_changes(filename):
    # This function is not used
    page_creation_times = {}
    with open(filename) as f:
        while True:
            try:
                page_name = next(f)
                if not re.match(WIKI_WORD, page_name.lstrip("!")):
                    print(f"Oops, {page_name} does not look like a wiki page name!")
                    while next(f).strip():
                        pass
                    continue
                timestamp = next(f)
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp))
                except ValueError:
                    timestamp = parser.parse(timestamp)
                try:
                    timestamp = pytz.utc.localize(timestamp)
                except ValueError:
                    pass
                if page_name.startswith("!"):
                    page_creation_times[page_name[1:]] = timestamp
                print("    ", timestamp)

                next(f)  # IP
                next(f)

            except StopIteration:
                return page_creation_times


def import_pages(pages_dir: str, diffs_dir: str, repo_dir: str, pagename=None):
    # TODO OK this is too simple, since it does not seem like git can show the log properly ordered.
    # I think we have to make the commits in the same order the changes happened.
    os.chdir(repo_dir)
    subprocess.run(["git", "init"])

    changes = []

    pagenames = [pagename] if pagename else os.listdir(pages_dir)

    with TemporaryDirectory() as tmpdir:
        for pagename in pagenames:
            if pagename == "changes.log":
                continue
            fullpath = os.path.join(pages_dir, pagename)
            if os.path.isdir(fullpath):
                continue
            diffpath = os.path.join(diffs_dir, f"{pagename}.diffs")
            if os.path.exists(diffpath):
                try:
                    with open(fullpath) as pf, open(diffpath) as df:
                        versions = get_versions(pagename, pf.read(), df.read(), tmpdir)
                except UnicodeDecodeError:
                    with open(fullpath, encoding="latin-1") as pf, open(diffpath, encoding="latin-1") as df:
                        versions = get_versions(pagename, pf.read(), df.read(), tmpdir)
            else:
                versions = []
            changes.extend(versions)

        # Now commit the changes in chronological order
        changes.sort(key=lambda c: c[0])
        for i, change in enumerate(changes):
            try:
                date, pagename, filename = change
                shutil.copyfile(filename, pagename)
                timestamp = date.timestamp()
                os.utime(pagename, (timestamp, timestamp))
                subprocess.run(["git", "add", pagename])
                if date:
                    env = os.environ.copy()
                    # Set the timestamp to the actual modification time
                    env["GIT_AUTHOR_DATE"] = date.strftime(GIT_DATE_FMT2)
                    subprocess.run([
                        "git", "commit",
                        "-m", f"Imported page '{pagename}', change {i}."
                    ], env=env)
                else:
                    subprocess.run([
                        "git", "commit",
                        "-m", f"Imported page '{pagename}', change {i}."
                    ])
            except RuntimeError as e:
                print(f"Failed to patch '{pagename}': {e}")
            except StopIteration:
                break


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 5:
        import_pages(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        import_pages(sys.argv[1], sys.argv[2], sys.argv[3])
