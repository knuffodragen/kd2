KD2 is a simple wiki. Pages are kept in a git repo.

Note: KD2 is a backwards compatible rewrite of a very old wiki, and therefore has its own markup and conventions.

  $ cd kd2
  $ python -m venv env
  $ env/bin/pip install -e .

KD2 can be configured either using environment variables...

  $ PAGES_DIR=/tmp/pages HOME_PAGE=SomePageName env/bin/uvicorn kd2:app

...or by creating a file called ".env" in the project root, containing the configuration variables:

  PAGES_DIR=/tmp/pages
  HOME_PAGE=SomePageName
